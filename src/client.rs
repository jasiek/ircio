extern crate irc;

use client::irc::client::prelude::{IrcClient, IrcReactor, Config};
use client::irc::client::ext::ClientExt;    

pub struct Client {
    client: IrcClient,
    reactor: IrcReactor,
    default_channel_name: String
}

impl Client {
    pub fn new() -> Client {
        let config = Config {
            server: Some("irc.freenode.net".to_owned()),
            channels: Some(vec!["#test-ircio".to_owned()]),
            nickname: Some("ircio-6969".to_owned()),
            use_ssl: Some(true),
            ..Config::default()
        };
        
        let mut reactor = IrcReactor::new().unwrap();
        let client = reactor.prepare_client_and_connect(&config).unwrap();
        client.identify().unwrap();
        let write_client = client.clone();

        reactor.register_client_with_handler(client, |_, message| {
            print!("{}", message);
            Ok(())
        });
        
        Client {
            reactor: reactor,
            client: write_client,
            default_channel_name: "#test-ircio".to_owned()
        }
    }

    pub fn run(&mut self) {
        self.reactor.run();
    }

    pub fn send_to_channel(&self, c: &str, m: &str) {
        self.client.send_privmsg(c, m).unwrap();
    }

    pub fn send_to_user(&self, u: &str, m: &str) {
        self.client.send_privmsg(u, m).unwrap();
    }

    pub fn send_to_default_channel(&self, m: &str) {
        self.client.send_privmsg(&self.default_channel_name, m).unwrap();
    }
}
