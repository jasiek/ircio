extern crate regex;

use self::regex::Regex;

#[derive(Debug)]
pub enum Command {
    None,
    ToUser(String, String),
    ToChannel(String, String),
    ToDefaultChannel(String),
}

impl PartialEq for Command {
    fn eq(&self, other: &Command) -> bool {
        match (self, other) {
            (Command::ToUser(u, m), Command::ToUser(uu, mm)) => u == uu && m == mm,
            (Command::ToChannel(c, m), Command::ToChannel(cc, mm)) => c == cc && m == mm,
            (Command::ToDefaultChannel(m), Command::ToDefaultChannel(mm)) => m == mm,
            _ => false
        }
    }
}

#[derive(Debug)]
pub struct CmdParser {
    lines: Vec<String>
}

impl CmdParser {
    pub fn new() -> Self {
        CmdParser{ lines: Vec::new() }
    }

    pub fn consume(&mut self, s: &str) {
        self.lines.push(s.to_owned());
    }

    pub fn next_command(&mut self) -> Command {
        let cmd = self.parselines();
        self.lines.remove(0);
        cmd
    }

    pub fn lines_in_buffer(&self) -> usize {
        self.lines.len()
    }

    fn parselines(&self) -> Command {
        let dm = Regex::new(r"(?m)^([a-z]+): ?(.*)$").unwrap(); // DM to user
        let ch = Regex::new(r"(?m)^#([a-z]+) +(.*)$").unwrap(); // To channel

        for line in &self.lines {
            if dm.is_match(&line) {
                let caps = dm.captures(&line).unwrap();
                let user = caps.get(1).unwrap().as_str();
                let m = caps.get(2).unwrap().as_str();
                return Command::ToUser(user.to_owned(), m.to_owned())
            }
            
            if ch.is_match(&line) {
                let caps = ch.captures(&line).unwrap();
                let chan = caps.get(1).unwrap().as_str();
                let m = caps.get(2).unwrap().as_str();
                return Command::ToChannel(chan.to_owned(), m.to_owned())
            }

            return Command::ToDefaultChannel(line.to_owned())
        }

        Command::None
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse() {
        let p = &mut CmdParser::new();
        p.consume("#chan message");
        p.consume("nick: message");
        p.consume("message");
        p.consume("leftover");

        let cmd = p.next_command();
        assert_eq!(Command::ToChannel(String::from("chan"), String::from("message")), cmd);
        let cmd = p.next_command();
        assert_eq!(Command::ToUser(String::from("nick"), String::from("message")), cmd);
        let cmd = p.next_command();
        assert_eq!(Command::ToDefaultChannel(String::from("message")), cmd);

        assert_eq!(p.lines_in_buffer(), 1);
    }
}
