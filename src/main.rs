extern crate clap;
use clap::{Arg, App};

mod cmdparser;
mod client;
mod plumbing;

#[allow(dead_code)]
fn read_config<'a>() -> clap::ArgMatches<'a> {
    let matches = App::new("ircio")
        .version("0.0.1")
        .about("Simple IRC client which connects to stdin/out, useful for piping stuff")
        .author("Jan Szumiec <jan.szumiec@gmail.com>")
        .arg(Arg::with_name("host")
             .short("h")
             .help("Connect to this host")
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("port")
             .short("p")
             .help("Port to use")
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("nick")
             .short("n")
             .help("Nick to use")
             .required(true)
             .takes_value(true))
        .arg(Arg::with_name("password")
             .short("P")
             .help("Password to send to nickserv")
             .takes_value(true))
        .get_matches();
    return matches;
}

fn main() {
    // let _config = read_config();
    // let parser = &mut cmdparser::CmdParser::new();
    // parser.consume("#channel everyone");
    // parser.consume("jasiek: hello");
    // let c = parser.next_command();
    // print!("{:?}", c);
    let mut client = client::Client::new();
    client.run();
}
