use cmdparser::*;
use client::*;
use std::io::{Stdout, Write, stdout};

pub struct ClientParserOutputMediator {
    client: Client,
    parser: CmdParser,
    writer: Stdout,
}

impl ClientParserOutputMediator {
    pub fn new(c: Client, p: CmdParser) -> Self {
        ClientParserOutputMediator {
            client: c,
            parser: p,
            writer: stdout()
        }
    }

    pub fn run(&mut self) {
        let cmd = self.parser.next_command();
        match cmd {
            Command::ToUser(u, m) => {
                self.handle_to_user(&u, &m)
            },
            Command::ToChannel(c, m) => {
                self.handle_to_channel(&c, &m)
            },
            Command::ToDefaultChannel(m) => {
                self.handle_to_default_channel(&m)
            },
            _ => { }
        }
    }

    // Incoming messages
    pub fn message_from_user(&mut self, u: &str, m: &str) {
        let what = format!("{}: {}", u, m);
        self.write_stdout(&what)
    }

    pub fn message_from_channel(&mut self, c: &str, m: &str) {
        let what = format!("#{} {}", c, m);
        self.write_stdout(&what)
    }

    // Outgoing messages
    fn handle_to_user(&mut self, u: &str, m: &str) {
        self.client.send_to_user(u, m)
    }

    fn handle_to_channel(&mut self, c: &str, m: &str) {
        self.client.send_to_channel(c, m)
    }

    fn handle_to_default_channel(&mut self, m: &str) {
        self.client.send_to_default_channel(m)
    }

    fn write_stdout(&mut self, what: &str) {
        self.writer.write(what.as_bytes());
    }
}

